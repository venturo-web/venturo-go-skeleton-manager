package structs

type GetRequestParams struct {
	Filter string `param:"filter" query:"filter"`
	Limit  int    `param:"limit" query:"limit"`
	Offset int    `param:"offset" query:"offset"`
}
