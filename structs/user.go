package structs

type User struct {
	Id       int    `form:"id" json:"id"`
	Name     string `form:"name" json:"name"`
	Foto     string `form:"foto" json:"foto"`
	Email    string `form:"email" json:"email"`
	Password string `form:"password" json:"password"`
}
