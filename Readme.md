# Go Skeleton manager

This is a skeleton manager for a Go application that uses Docker to build and run the application.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- [Docker](https://docs.docker.com/get-docker/) installed on your local machine.

## Getting Started

These instructions will help you get your Go application up and running using the Docker image.

1. **Build the Docker Image:**

   ```bash
   docker build -t go-skeleton-manager .