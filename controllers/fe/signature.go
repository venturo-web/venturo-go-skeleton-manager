package controllers

import (
	"github.com/labstack/echo/v4"
	"go-skeleton-manager/helpers"
	"go-skeleton-manager/rmqauto"
	"go-skeleton-manager/structs"
	"log"
	"net/http"
)

func Signature(e echo.Context) error {
	log.Println("Starting process create signature")

	/* set response struct */
	response := new(structs.JSONResponse)

	/* Binding Request to struct */
	var req structs.User
	if err := e.Bind(&req); err != nil {
		/* Log Handle error */
		helpers.HandleError("Error on binding request", err.Error())

		response.StatusCode = http.StatusBadRequest
		response.Message = "Bad Request"
		response.Data = nil
		return e.JSON(response.StatusCode, response)
	}

	/* Making connection to rabbit default module & publish a command */
	status, message, data := rmqauto.RequestCommand(
		"create-user",
		"",
		req,
		true,
	)

	/* Check if RabbitMQ replay success */
	if data == nil {
		/* Generate response */
		response.StatusCode = int(status.(float64))
		response.Message = message.(string)
		response.Data = nil
		/* Set response data to nil, if error */
		return e.JSON(response.StatusCode, response)
	}

	/* Generate response */
	response.StatusCode = int(status.(float64))
	response.Message = message.(string)
	response.Data = data.(map[string]interface{})["Data"]

	/* Set response data, then return the request */
	return e.JSON(response.StatusCode, response)
}
