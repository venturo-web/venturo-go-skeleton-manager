package controllers

import (
	"github.com/labstack/echo/v4"
	"go-skeleton-manager/helpers"
	"go-skeleton-manager/rmqauto"
	"go-skeleton-manager/structs"
	"log"
	"net/http"
	"strconv"
)

// GetUser is for get list data of user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @payload JSON
 *
 * @return JSON
 */
func GetUser(e echo.Context) error {
	log.Println("Starting process get list user")

	/* set response struct */
	response := new(structs.JSONResponse)

	/* Binding Request to struct */
	var req structs.GetRequestParams
	if err := e.Bind(&req); err != nil {

		/* Log Handle error */
		helpers.HandleError("Error on binding request", err.Error())

		response.StatusCode = http.StatusBadRequest
		response.Message = "Bad Request"
		response.Data = nil
		return e.JSON(response.StatusCode, response)
	}

	/* Making connection to rabbit default module & publish a command */
	status, message, data := rmqauto.RequestCommand(
		"get-list-user",
		"",
		req,
		true,
	)

	/* Check if RabbitMQ replay success */
	if data == nil {
		/* Generate response */
		response.StatusCode = int(status.(float64))
		response.Message = message.(string)
		response.Data = nil
		/* Set response data to nil, if error */
		return e.JSON(response.StatusCode, response)
	}

	/* Generate response */
	response.StatusCode = int(status.(float64))
	response.Message = message.(string)
	response.Data = data.(map[string]interface{})["Data"]

	/* Set response data, then return the request */
	return e.JSON(response.StatusCode, response)
}

// GetDetailUser is for get detail data of user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @payload JSON
 *
 * @return JSON
 */
func GetDetailUser(e echo.Context) error {
	log.Println("Starting process get detail user")

	/* set response struct */
	response := new(structs.JSONResponse)

	/* Get param id */
	Id, _ := strconv.Atoi(e.Param("id"))

	/* Making connection to rabbit default module & publish a command */
	status, message, data := rmqauto.RequestCommand(
		"get-detail-user",
		"",
		map[string]interface{}{
			"id": Id,
		},
		true,
	)

	/* Check if RabbitMQ replay success */
	if data == nil {
		/* Generate response */
		response.StatusCode = int(status.(float64))
		response.Message = message.(string)
		response.Data = nil
		/* Set response data to nil, if error */
		return e.JSON(response.StatusCode, response)
	}

	/* Generate response */
	response.StatusCode = int(status.(float64))
	response.Message = message.(string)
	response.Data = data.(map[string]interface{})["Data"]

	/* Set response data, then return the request */
	return e.JSON(response.StatusCode, response)
}

// CreateUser is for create data a user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @payload JSON
 *
 * @return JSON
 */
func CreateUser(e echo.Context) error {
	log.Println("Starting process create user")

	/* set response struct */
	response := new(structs.JSONResponse)

	/* Binding Request to struct */
	var req structs.User
	if err := e.Bind(&req); err != nil {
		/* Log Handle error */
		helpers.HandleError("Error on binding request", err.Error())

		response.StatusCode = http.StatusBadRequest
		response.Message = "Bad Request"
		response.Data = nil
		return e.JSON(response.StatusCode, response)
	}

	/* Making connection to rabbit default module & publish a command */
	status, message, data := rmqauto.RequestCommand(
		"create-user",
		"",
		req,
		true,
	)

	/* Check if RabbitMQ replay success */
	if data == nil {
		/* Generate response */
		response.StatusCode = int(status.(float64))
		response.Message = message.(string)
		response.Data = nil
		/* Set response data to nil, if error */
		return e.JSON(response.StatusCode, response)
	}

	/* Generate response */
	response.StatusCode = int(status.(float64))
	response.Message = message.(string)
	response.Data = data.(map[string]interface{})["Data"]

	/* Set response data, then return the request */
	return e.JSON(response.StatusCode, response)
}

// UpdateUser is for update data a user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @payload JSON
 *
 * @return JSON
 */
func UpdateUser(e echo.Context) error {
	log.Println("Starting process update user")

	/* set response struct */
	response := new(structs.JSONResponse)

	var req structs.User
	/* Binding Request to struct */
	if err := e.Bind(&req); err != nil {

		/* Log Handle error */
		helpers.HandleError("Error on binding request", err.Error())

		response.StatusCode = http.StatusBadRequest
		response.Message = "Bad Request"
		response.Data = nil
		return e.JSON(response.StatusCode, response)
	}

	/* Get param id and bind to struct */
	Id, _ := strconv.Atoi(e.Param("id"))
	req.Id = Id

	/* Making connection to rabbit default module & publish a command */
	status, message, data := rmqauto.RequestCommand(
		"update-user",
		"",
		req,
		true,
	)

	/* Check if RabbitMQ replay success */
	if data == nil {
		/* Generate response */
		response.StatusCode = int(status.(float64))
		response.Message = message.(string)
		response.Data = nil
		/* Set response data to nil, if error */
		return e.JSON(response.StatusCode, response)
	}

	/* Generate response */
	response.StatusCode = int(status.(float64))
	response.Message = message.(string)
	response.Data = data.(map[string]interface{})["Data"]

	/* Set response data, then return the request */
	return e.JSON(response.StatusCode, response)
}

// DeleteUser is for delete data a user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @payload JSON
 *
 * @return JSON
 */
func DeleteUser(e echo.Context) error {
	log.Println("Starting process delete user")

	/* set response struct */
	response := new(structs.JSONResponse)

	/* Get param id and bind to struct */
	Id, _ := strconv.Atoi(e.Param("id"))

	/* Making connection to rabbit default module & publish a command */
	status, message, data := rmqauto.RequestCommand(
		"delete-user",
		"",
		map[string]interface{}{
			"id": Id,
		},
		true,
	)

	/* Check if RabbitMQ replay success */
	if data == nil {
		/* Generate response */
		response.StatusCode = int(status.(float64))
		response.Message = message.(string)
		response.Data = nil
		/* Set response data to nil, if error */
		return e.JSON(response.StatusCode, response)
	}

	/* Generate response */
	response.StatusCode = int(status.(float64))
	response.Message = message.(string)
	response.Data = data.(map[string]interface{})["Data"]

	/* Set response data, then return the request */
	return e.JSON(response.StatusCode, response)
}
