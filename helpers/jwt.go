package helpers

import (
	"fmt"
	"os"
	"strings"

	"github.com/golang-jwt/jwt"
)

func DecodeTokenJwt(tokenStr string) (jwt.MapClaims, bool) {

	// check if token contains Bearer
	if strings.Contains(tokenStr, "Bearer") {
		tokenStr = strings.Replace(tokenStr, "Bearer ", "", -1)
	}

	hmacSecretString := os.Getenv("JWT_SECRET")
	hmacSecret := []byte(hmacSecretString)
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// check token signing method etc
		return hmacSecret, nil
	})

	if err != nil {
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	} else {
		fmt.Println("Invalid Jwt")
		return nil, false
	}
}

// GetUserIdFromJwtToken get user id from jwt token
func GetUserIdFromJwtToken(tokenStr string) (int32, bool) {
	claims, ok := DecodeTokenJwt(tokenStr)
	if ok {
		return int32(claims["UserId"].(float64)), true
	} else {
		return 0, false
	}
}
