package helpers

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
)

// HandleError is for handle error on terminal
/**
 * @author Mahendra Dwi Purwanto
 *
 * @message string, err interface{}
 */
func HandleError(message string, err interface{}) {
	log.Println("========== Start Error Message ==========")
	log.Println("Message => " + message + ".")
	if err != nil {
		log.Println("Error => ", err)
	}
	log.Println("========== End Of Error Message ==========")
	log.Println()
}

// MarshalUnmarshal is for marshal and unmarshal data
/**
 * @author Mahendra Dwi Purwanto
 *
 * @param interface{}
 *
 * @return result interface{}
 */
func MarshalUnmarshal(param interface{}, result interface{}) error {
	paramByte, err := json.Marshal(param)
	if err != nil {
		log.Println("Error marshal", err.Error())
		return err
	}

	err = json.Unmarshal(paramByte, &result)
	if err != nil {
		log.Println("Error unmarshal", err.Error())
		return err
	}

	return nil
}

// MapToString is for convert map to string
/**
 * @author Mahendra Dwi Purwanto
 *
 * @data map[string]interface{}
 *
 * @return string
 */
func MapToString(data map[string]interface{}) string {
	b := new(bytes.Buffer)
	for key, value := range data {
		_, err := fmt.Fprintf(b, "\"%s\":\"%s\"\n", key, value)
		if err != nil {
			return ""
		}
	}
	return b.String()
}

// RandomByte is for generate random byte
/**
 * @author Mahendra Dwi Purwanto
 *
 * @b []byte
 *
 * @return string
 */
func RandomByte(n int) string {
	b := make([]byte, n)
	_, _ = rand.Read(b)

	return base64.URLEncoding.EncodeToString(b)
}
