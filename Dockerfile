# Use the official Golang 1.20 image as the builder stage
FROM golang:1.20 AS builder

# Set the working directory inside the container
WORKDIR /app

# Copy the Go module files and download dependencies
COPY go.mod go.sum ./
RUN go mod download

# Copy the source code into the container
COPY . .

# Build the Go application with CGO disabled for a static binary
RUN CGO_ENABLED=0 go build -o myapp

# Create a minimal image for the final stage
FROM alpine:latest

# Set the working directory in the final stage
WORKDIR /app

# Copy the binary from the builder stage
COPY --from=builder /app/myapp .

# Expose the port your application listens on (adjust as needed)
EXPOSE 8050

# Command to start your application
CMD ["./myapp"]