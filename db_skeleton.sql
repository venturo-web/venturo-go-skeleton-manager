-- Adminer 4.8.1 MySQL 8.1.0 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Foto` varchar(50) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `CreatedAt` varchar(25) DEFAULT NULL,
  `CreatedBy` int DEFAULT NULL,
  `UpdatedAt` varchar(25) DEFAULT NULL,
  `UpdatedBy` int DEFAULT NULL,
  `DeletedAt` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `user` (`Id`, `Name`, `Foto`, `Email`, `Password`, `CreatedAt`, `CreatedBy`, `UpdatedAt`, `UpdatedBy`, `DeletedAt`) VALUES
(1,	'Mahendra',	'https://profile.png',	'mahendra@gmail.com',	'12344321',	'2023-11-24 09:38:09',	NULL,	NULL,	NULL,	NULL);

-- 2023-11-24 02:39:14
