package main

import (
	"go-skeleton-manager/helpers"
	"go-skeleton-manager/routes"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// Load environment variables from the .env file.
	errLoadingEnvFile := godotenv.Load()
	if errLoadingEnvFile != nil {
		helpers.HandleError("error loading the .env file", errLoadingEnvFile)
	}

	// Create a new Echo instance.
	e := echo.New()

	// Enable Cross-Origin Resource Sharing (CORS) middleware with specific configurations.
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"}, // Allow requests from any origin.
		AllowCredentials: true,          // Allow including credentials in requests.
		AllowMethods: []string{ // Allow specified HTTP methods.
			http.MethodGet,
			http.MethodHead,
			http.MethodPut,
			http.MethodPatch,
			http.MethodPost,
			http.MethodDelete,
		},
	}))

	// Build and register the routes for the Echo instance.
	routes.Build(e)

	// Start the Echo web server on the specified port from environment variables.
	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))
}
