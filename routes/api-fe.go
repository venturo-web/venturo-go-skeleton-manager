package routes

import (
	controllers "go-skeleton-manager/controllers/fe"
	"go-skeleton-manager/routes/middleware"

	"github.com/labstack/echo/v4"
)

var prefix = "/api/v1"

func RouteWebAppsServices(e *echo.Echo) {
	r := e.Group(prefix)

	// r.GET("/signature", controllers.Signature) with middleware of signatureRequest
	r.POST("/signature", middleware.SignatureRequest(controllers.Signature))

	/* ------------------ CRUD User  ------------------------------- */
	r.GET("/user", controllers.GetUser)
	r.GET("/user/:id", controllers.GetDetailUser)
	r.POST("/user", controllers.CreateUser)
	r.PUT("/user/:id", controllers.UpdateUser)
	r.DELETE("/user/:id", controllers.DeleteUser)
}
