// create jwt authentification
// check valid, check expired, and check signature

package middleware

import (
	"go-skeleton-manager/helpers"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

func Auth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(e echo.Context) error {
		if authorization := e.Request().Header.Get("Authorization"); authorization != "" {
			log.Println("========== Request log ==========")
			log.Println()
			log.Printf("URL: %s%s", e.Request().Host, e.Request().URL.Path)
			log.Println("=========== Request Log =============")
			log.Println()

			authorizationToken := strings.Split(authorization, " ")

			if len(authorizationToken) != 2 {
				return helpers.HandleResponse(e, http.StatusUnauthorized, "Unauthorized", nil)
			}

			if authorizationToken[0] != "Bearer" {
				return helpers.HandleResponse(e, http.StatusUnauthorized, "Unauthorized", nil)
			}

			token, err := jwt.Parse(authorizationToken[1], func(token *jwt.Token) (interface{}, error) {
				_, _ = token.Method.(*jwt.SigningMethodHMAC)

				return []byte(os.Getenv("JWT_SECRET")), nil
			})

			if err != nil {
				return helpers.HandleResponse(e, http.StatusUnauthorized, "authorization token credentials do not match", nil)
			}

			claims, ok := token.Claims.(jwt.MapClaims)

			if !ok || !token.Valid {
				return helpers.HandleResponse(e, http.StatusUnauthorized, "invalid authorization token credentials", nil)
			}

			tokenExpired, _ := time.Parse("2006-01-02 15:04:05", claims["Expired"].(string))
			currentTime, _ := time.Parse("2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05"))

			if currentTime.After(tokenExpired) {
				return helpers.HandleResponse(e, http.StatusUnauthorized, "Token Expired", nil)
			}

			e.Set("Authorization", claims)
			return next(e)
		}
		return helpers.HandleResponse(e, http.StatusUnauthorized, "Unauthorized", nil)
	}
}

func AnyAccess(access []string) func(next echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(e echo.Context) error {
			if e.Get("Authorization") != nil {
				claims := e.Get("Authorization").(jwt.MapClaims)
				permissions := claims["Permission"].([]interface{})
				for _, acc := range access {
					for _, val := range permissions {
						permission := val.(string)
						if permission == acc {
							return next(e)
						}
					}
				}
			}
			return helpers.HandleResponse(e, http.StatusUnauthorized, "Unauthorized", nil)
		}
	}
}

func CheckExpirationToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(e echo.Context) error {
		dataToken := e.Get("Authorization").(map[string]interface{})
		tokenExpired, _ := time.Parse("2006-01-02 15:04:05", dataToken["Expired"].(string))
		currentTime, _ := time.Parse("2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05"))
		if currentTime.After(tokenExpired) {
			return helpers.HandleResponse(e, http.StatusUnauthorized, "Token Expired", nil)
		}
		return next(e)
	}
}

func ParseJWTToken(token string) map[string]interface{} {
	tokenMap, _ := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		_, _ = token.Method.(*jwt.SigningMethodHMAC)

		return []byte(os.Getenv("JWT_SECRET")), nil
	})

	claims, _ := tokenMap.Claims.(jwt.MapClaims)

	return claims
}
