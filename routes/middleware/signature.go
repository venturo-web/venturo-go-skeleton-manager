package middleware

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"go-skeleton-manager/helpers"
	"go-skeleton-manager/system"
	"io/ioutil"
	"net/http"
	"os"
)

func SignatureRequest(next echo.HandlerFunc) echo.HandlerFunc {
	return func(e echo.Context) error {
		if signature := e.Request().Header.Get("Signature"); signature != "" {

			// Example RSA private and public keys in PEM format
			publicKeyPEM := os.Getenv("PUBLIC_KEY_SIGNATURE_REQUEST")

			// Parse the public key
			publicKey, err := system.ParseRSAPublicKey(publicKeyPEM)
			if err != nil {
				fmt.Println("Error parsing public key:", err)
				return helpers.HandleResponse(e, http.StatusUnprocessableEntity, "Error parsing public key", err)
			}

			// Read the request body
			body, err := ioutil.ReadAll(e.Request().Body)
			if err != nil {
				fmt.Println("Error reading request body:", err)
				return helpers.HandleResponse(e, http.StatusUnprocessableEntity, "Error reading request body", err)
			}

			// Unmarshal the JSON
			var requestBody map[string]interface{}
			err = json.Unmarshal(body, &requestBody)
			if err != nil {
				fmt.Println("Error decoding JSON:", err)
				return helpers.HandleResponse(e, http.StatusUnprocessableEntity, "Error decoding JSON", err)
			}

			// Convert to string
			payloadString, err := json.Marshal(requestBody)
			if err != nil {
				fmt.Println("Error json encode body request:", err)
				return helpers.HandleResponse(e, http.StatusUnprocessableEntity, "Error json encode body request", err)
			}

			// Verify RSA signature
			isValid, err := system.VerifyRSASignature(publicKey, string(payloadString), signature)
			if err != nil {
				fmt.Println("Invalid signature: ", err)
				// generate formal message if invalid signature
				return helpers.HandleResponse(e, http.StatusUnauthorized, "Authentication failed due to an invalid signature.", err)
			}

			fmt.Println("Signature is valid:", isValid)

			return next(e)
		}
		return helpers.HandleResponse(e, http.StatusUnauthorized, "Signature not provided", nil)
	}
}
