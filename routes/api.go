package routes

import (
	"github.com/labstack/echo/v4"
)

// Build returns the application routes
func Build(e *echo.Echo) {
	// e.Use(middleware.JWT([]byte(os.Getenv("JWT_SECRET"))))

	// route for mobile apps
	RouteMobileAppsServices(e)

	// route for web apps
	RouteWebAppsServices(e)
}
